import java.util.LinkedList;


public class LongStack {

    private final LinkedList<Long> ls;

    public static void main(String[] args) {

//        System.out.println (interpret("-234"));
//         System.out.println (interpret("     "));
//         System.out.println (interpret("-234 @"));
//         System.out.println (interpret("-234 -"));
//         System.out.println (interpret("-234 56"));
         System.out.println (interpret("-234 56 #"));

    }

    LongStack() {
        ls = new LinkedList<>();
    }

    @Override
    // Ideid voetud siit
    //https://enos.itcollege.ee/~jpoial/algorithms/examples/IntStack.java
    public Object clone() throws CloneNotSupportedException {

        LongStack temp = new LongStack();
        if (!stEmpty()) {
            for (int i = ls.size() - 1; i >= 0; i--) {
                temp.push(ls.get(i));
            }
        }
        return temp;
    }

    public boolean stEmpty() {
        return ls.isEmpty();
    }

    public void push(long a) {
        ls.push(a);
    }

    public long pop() {
        if (stEmpty()) {
            throw new IndexOutOfBoundsException("Can't pop from empty list");
        }
        return ls.pop();
    }

    public void op(String s) {
        if (ls.size() < 2) {
            throw new IndexOutOfBoundsException(String.format("Not enough numbers in the list. String value: [%s]", s));
        }
        if (!(s.equals("-") || s.equals("+") || s.equals("*") || s.equals("/"))) {
            throw new IllegalArgumentException(String.format("Input [%s]] is not a proper operator", s));
        }
        long num1 = pop();
        long num2 = pop();
        if (s.equals("-")) push(num2 - num1);
        if (s.equals("+")) push(num2 + num1);
        if (s.equals("*")) push(num2 * num1);
        if (s.equals("/")) push(num2 / num1);
    }

    public long tos() {
        if (stEmpty()) {
            throw new IndexOutOfBoundsException("No numbers in the list");
        }
        return ls.get(0);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof LongStack)) {
            throw new ClassCastException("Comparable not of type LongStack");
        }
        LongStack temp = (LongStack) o;
        if (ls.size() != temp.ls.size()) return false;

        for (int i = 0; i < ls.size(); i++) {
            if (!ls.get(i).equals(temp.ls.get(i))) return false;
        }
        return true;
    }

    @Override
    public String toString() {
        if (stEmpty()) {
            return "Empty list";
        }

        StringBuilder sb = new StringBuilder();
        for (Long l : ls) {
            sb.append(l).append(" ");
        }
        return sb.reverse().toString();
    }

    // https://stackabuse.com/java-check-if-string-is-a-number/
    public static long interpret(String pol) {
        String originalString = pol;
        if (pol == null) {
            throw new NullPointerException("Input must be string. Current value 'null'");
        }
        pol = pol.trim();
        if (pol.equals("")) {
            throw new IllegalArgumentException("Input is empty string");
        }

        LongStack ls = new LongStack();
        long longValue;
        String el;
        String[] tempList = pol.split(" ");

        for (String s : tempList) {
            el = s.trim();

            try {
                longValue = Long.parseLong(el);
                ls.push(longValue);
            } catch (NumberFormatException e) {

                if (!el.equals("")) {
                    if (!(el.equals("-") || el.equals("+") || el.equals("*") || el.equals("/"))) {

                        throw new IllegalArgumentException(String.format("Input string \"%s\" can not be solved. Invalid character: " + el, originalString));
                    }
                    try {
                        ls.op(el);
                    } catch (RuntimeException err) {
                        throw new RuntimeException(String.format("Input string \"%s\" can not be solved. Too few numbers", originalString));
                    }
                }
            }
        }
        if (ls.ls.size() > 1) {
            throw new RuntimeException(String.format("Input string \"%s\" can not be solved. Too many numbers", originalString));
        }
        return ls.pop();
    }
}